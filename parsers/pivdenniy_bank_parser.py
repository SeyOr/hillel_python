from typing import List

from bs4 import BeautifulSoup
import requests
import re

from parsers.bank import Bank


class PivdenniyParser(Bank):

    def __init__(self, currencies: List, bank_url: str, bank_id: int):
        self.__currencies = {
            element[2]: (element[0], element[1]) for element in currencies
        }
        self.__bank_url = bank_url
        self.__bank_id = bank_id

    def __get_html(self):
        resp = requests.get(self.__bank_url)
        return resp.text

    def get_currency_rate(self):
        currency_rate = {
            'bank_id': self.__bank_id,
            'rate': []
        }

        html = self.__get_html()
        soup = BeautifulSoup(html, 'lxml')

        contents = soup.find('div', {'class': 'course-table'}).find_all('span')

        for line in contents:

            if line['class'][0] == 'course-table__code':
                currency_name = line.get_text().strip()
                buy, sell, nbu = 0, 0 ,0

            elif line['class'][0] == 'course-table__rate':
                if (buy+sell+nbu) == 0 :
                    buy = float(line.get_text().strip())
                elif (sell+nbu) == 0 :
                    sell = float(line.get_text().strip())
                elif nbu == 0:
                    nbu = float(line.get_text().strip())
            
            else:
                continue
 
            if nbu > 0:
                currency_rate['rate'].append(
                    {
                        'currency_id': self.__currencies.get(currency_name.lower())[0],
                        'purchase': buy,
                        'sale': sell
                    }
                )

        return currency_rate


if __name__ == '__main__':
    from connector import DbUtils
    from pprint import pprint

    db = DbUtils()
    db.connect()
    currencies = db.get_currencies()
    bank_id, bank_name, bank_url = db.get_bank_by_id(6)
    db.close()
    parser = PivdenniyParser(currencies, bank_url, bank_id)
    pprint(parser.get_currency_rate())
