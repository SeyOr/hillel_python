from django.dispatch import Signal

my_Sig = Signal()

def func_1(*args, **kwargs):
    print("name 1",args, kwargs)


def func_2(*args, **kwargs):
    print("name 2",args, kwargs)


def func_3(*args, **kwargs):
    print("name 3",args, kwargs)


my_Sig.connect(func_1)
my_Sig.connect(func_2)
my_Sig.connect(func_3)

my_Sig.send(sender="test", val1=45, val2=22, val=object, val3="fsa")