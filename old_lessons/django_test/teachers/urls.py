from django.urls import path
from teachers.forms import TeacherCreateForm

from teachers.views import TeacherListView, CreateTeacherView, TeacherUpdateView, delete_teacher


app_name = 'teachers'

urlpatterns = [
    path('', TeacherListView.as_view(), name='list'),
    path('create/', CreateTeacherView.as_view(), name='create'),
    path('update/<int:pk>/', TeacherUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', delete_teacher, name='delete'),
]
