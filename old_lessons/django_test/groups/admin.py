from django.contrib import admin
from django.forms import models
from django.forms.models import BaseModelFormSet

from .models import Group
from students.models import Students


class StudentsInlineTable(admin.TabularInline):
    model = Students

    
    fields = [
        'last_name',
        'first_name',
        'email',
        'age',
    ]

    readonly_fields = fields
    show_change_link = True             # edit in Student model
    extra = 0               # default = 3
    # template = "groups/custom.html"

    autocomplete_fields = ('group',)



class GroupAdmin(admin.ModelAdmin):
    list_display = [
        'group_name',
        'creation_date',
        'headman',
    ]

    fields = [
        'group_name',
        'creation_date',
        'headman',
        'teachers',
    ]
    search_fields = ('last_name',)

    inlines = [StudentsInlineTable,]



admin.site.register(Group, GroupAdmin)

