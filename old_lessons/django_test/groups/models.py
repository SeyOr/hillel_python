import datetime
import random

from django.db import models

# Create your models here.
from faker import Faker
from core.models import get_obj

from teachers.models import Teacher
from courses.models import Course

class Group(models.Model):

    group_name = models.CharField(max_length=100)
    creation_date = models.DateField(null=False, default=datetime.date.today)
    subject = models.CharField(max_length=100, null=False)

    headman = models.OneToOneField(
        'students.Students',
        on_delete=models.SET_NULL,
        null=True,
        related_name='headed_group'
    )

    teachers = models.ManyToManyField(
        to=Teacher,
        related_name='groups'
    )    

    def __str__(self):
        return f'{self.id}, {self.group_name}, {self.subject}, {self.creation_date}'

    @staticmethod
    def generate_groups(count):
        faker = Faker()
        for _ in range(count):
            st = Group(
                group_name=faker.catch_phrase(),
                creation_date=faker.date_between(start_date='-5y', end_date='-1y'),
                subject=str(get_obj(Course).subject)
            )
            st.save()
