from groups.views import GroupListView, GroupUpdateView, CreateGroupView, delete_Group
from django.urls import path

#  from .views import GroupUpdateView

app_name = 'groups'

urlpatterns = [
    path('', GroupListView.as_view(), name='list'),
    path('create/', CreateGroupView.as_view(), name='create'),
    path('update/<int:pk>/', GroupUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', delete_Group, name='delete'),
]