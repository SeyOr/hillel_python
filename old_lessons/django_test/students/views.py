from typing import Reversible
from django.contrib import messages
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls.base import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

from students.forms import StudentCreateForm, StudentUpdateForm, StudentsFilter
from students.models import Students

from webargs import fields
from webargs.djangoparser import use_args


class StudentListView(LoginRequiredMixin, ListView):
    paginate_by = 10
    model = Students
    template_name = 'students/list.html'

    def get_filter(self):
        return StudentsFilter(
            data=self.request.GET,
            queryset=self.model.objects.all().select_related('group', 'headed_group')
        )

    def get_queryset(self):
        return self.get_filter().qs

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_filter'] = self.get_filter()

        return context
 

class CreateStudentView(LoginRequiredMixin, CreateView):
    model = Students
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students/create.html'

    def form_valid(self, form):
        result = super().form_valid(form)
        name = form.cleaned_data["last_name"]+" "+form.cleaned_data["first_name"]
        messages.success(self.request, f'Student {name} been created.')

        return result

class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Students
    form_class = StudentUpdateForm
    success_url = reverse_lazy('students:list')
    template_name = 'students/update.html'


@login_required
def delete_student(request, pk):
    student = get_object_or_404(Students, id=pk)
    if request.method == 'POST':
        student.delete()
        return HttpResponseRedirect(reverse('students:list'))

    return render(
        request=request,
        template_name='students/delete.html',
        context={
            'student': student
        }
    )


class StudentDeleteView(LoginRequiredMixin, DeleteView):
    model = Students
    template_name = 'students/delete.html'
    success_url = reverse_lazy('students:list')
