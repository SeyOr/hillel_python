from django.core.management.base import BaseCommand

from students.models import Students


class Command(BaseCommand):
    help = "Studnets generator"  # noqa

    def add_arguments(self, parser):
        parser.add_argument(
            'number',
            nargs='+',
            type=int
        )
        return super().add_arguments(parser)


    def handle(self, *args, **options):
          Students.generate(count=options['number'][0])  # noqa
          self.stdout.write(self.style.SUCCESS('Successfully add "%s" students' % options['number'][0]))
