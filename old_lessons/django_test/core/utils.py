from random import random

from groups.models import Group
from teachers.models import Teacher

from django.db.models import Max

import teachers
import groups

def format_list(lst):
    return '<br>'.join(lst)


def format_records(lst, type_name):
    if len(lst) == 0:
        return '(Emtpy recordset)'

    result = []

    for elem in lst:
        formatted_elem = f'<a href="/{type_name}/update/{elem.id}">EDIT</a> {elem}'
        result.append(formatted_elem)

    return '<br>'.join(result)

@classmethod
def get_random(cls):
    max_id = cls.objects.all().aggregate(max_id=Max("id"))['max_id']
    while True:
        pk = random.randint(1, max_id)
        category = cls.objects.filter(pk=pk).first()
        if category:
            return category
            

def get_random_teacher():
    max_id = teachers.models.Teacher.objects.all().aggregate(max_id=Max("id"))['max_id']
    while True:
        pk = random.randint(1, max_id)
        category = teachers.models.Group.objects.filter(pk=pk).first()
        if category:
            return category


