from django.contrib.auth import views
from django.urls import path
from django.urls.base import reverse_lazy
from django.views.generic.base import View

from .views import AccountPasswordChange, AccountRegistrationView, AccountLoginView, AccountLogoutView, AccountUpdateView

app_name = 'accounts'

urlpatterns = [
    path('registration/', AccountRegistrationView.as_view(), name='registration'),
    path('login/', AccountLoginView.as_view(), name='login'),
    path('logout/', AccountLogoutView.as_view(), name='logout'),
    path('change_password/', AccountPasswordChange.as_view(), name='change_password'),
    path('profile_update/', AccountUpdateView.as_view(), name='profile_update'),


    path(
        'password-reset/', 
        views.PasswordResetView.as_view(
            template_name='accounts/password_reset_form.html',
            success_url=reverse_lazy('accounts:password_reset_done')
            ),
        name='password_reset'
        ),


    path(
        'password-reset-confirm/<uidb64>/<token>/', 
        views.PasswordResetConfirmView.as_view(
            template_name='accounts/password_reset_confirm.html'
            ),
        name='password_reset_confirm'
        ),


    path(
        'password-reset-done/', 
        views.PasswordResetDoneView.as_view(
            template_name='accounts/password_reset_done.html',
            ),
        name='password_reset_done'
        ),

    
    


]