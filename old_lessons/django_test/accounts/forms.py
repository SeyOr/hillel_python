from django.contrib.auth.forms import PasswordResetForm, UserChangeForm, UserCreationForm, PasswordChangeForm
from django.forms.models import ModelForm

from accounts.models import Profile


class AccountRegistrationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email'
        ]

class AccountUpdateForm(UserChangeForm):
    password = None

    class Meta(UserChangeForm.Meta):
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ]


class PasswordChange(PasswordChangeForm):
        fields = [
            'old_password',
            'new_password1',
            'new_password2'

        ]


class UserPasswordReset(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super(UserPasswordReset, self).__init__(*args, **kwargs) 

    fields = [
        'user',
        'email_message'
    ]


class AccountUpdateProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ['avatar',
                'interests']

