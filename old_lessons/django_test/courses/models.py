import datetime
import random

from django.db import models

# Create your models here.
from faker import Faker


class Course(models.Model):
    SUBJECT = ["Math", "Literature", "Art", "Science", "Photos"]

    course_name = models.CharField(max_length=100)
    creation_date = models.DateField(null=False, default=datetime.date.today)
    subject = models.CharField(max_length=100, null=False)

    students = models.ForeignKey(
        'students.Students', 
        on_delete=models.SET_NULL, 
        null=True, 
        related_name='course'
        )

    teachers = models.ForeignKey(
        'teachers.Teacher', 
        on_delete=models.SET_NULL, 
        null=True, 
        related_name='teacher'
        )

    group = models.OneToOneField(
        'groups.Group',
        on_delete=models.SET_NULL,
        null=True,
        related_name='group_course'
    )


    def __str__(self):
        return f'{self.id}, {self.course_name}, {self.subject}, {self.creation_date}'

    @staticmethod
    def generate_course(count):
        faker = Faker()
        for _ in range(count):
            st = Course(
                course_name=faker.catch_phrase(),
                creation_date=faker.date_between(start_date='-5y', end_date='-1y'),
                subject=random.choice(Course.SUBJECT)
            )
            st.save()