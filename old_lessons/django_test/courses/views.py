from typing import Reversible
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls.base import reverse, reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView

from courses.forms import CourseCreateForm, CourseUpdateForm, CourseFilter
from courses.models import Course

from webargs import fields
from webargs.djangoparser import use_args


class CourseListView(LoginRequiredMixin, ListView):
    model = Course
    template_name = 'courses/list.html'

    def get_queryset(self):
        obj_list = CourseFilter(
            data=self.request.GET,
            queryset=self.model.objects.all()
        )

        return obj_list


class CreateCourseView(LoginRequiredMixin, CreateView):
    model = Course
    form_class = CourseUpdateForm
    success_url = reverse_lazy('courses:list')
    template_name = 'courses/create.html'


class CourseUpdateView(LoginRequiredMixin, UpdateView):
    model = Course
    form_class = CourseUpdateForm
    success_url = reverse_lazy('courses:list')
    template_name = 'courses/update.html'


class CoursetDeleteView(LoginRequiredMixin, DeleteView):
    model = Course
    template_name = 'courses/delete.html'
    success_url = reverse_lazy('courses:list')




def delete_Course(request, pk):
    course = get_object_or_404(Course, id=pk)
    if request.method == 'POST':
        course.delete()
        return HttpResponseRedirect(reverse('courses:list'))

    return render(
        request=request,
        template_name='courses/delete.html',
        context={
            'course': Course
        }
    )