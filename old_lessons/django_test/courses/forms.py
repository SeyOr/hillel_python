from django.forms import ModelForm

import django_filters
from courses.models import Course


class CourseCreateForm(ModelForm):
    class Meta:
        model = Course
        fields = [
            'course_name', 
            'subject', 
            'creation_date']


class CourseUpdateForm(CourseCreateForm):
    class Meta(CourseCreateForm.Meta):
        fields = [
            'course_name',
            'subject',
            'creation_date',
            'teachers',
            'students'
        ]


class CourseFilter(django_filters.FilterSet):
    class Meta:
        model = Course
        fields = {
            'subject': ['exact','icontains'],
            'course_name': ['exact', 'icontains'],
        }
