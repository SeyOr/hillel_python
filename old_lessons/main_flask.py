from flask import Flask, request, Response
from marshmallow.validate import Length
from webargs.flaskparser import use_kwargs
from webargs import validate, fields
from less4.less4_home import *

app = Flask(__name__)


@app.route("/get_unique_names")
def get_unique_names():
    return str(unique_names())


@app.route("/get_tracks_count")
def get_tracks_count():
    return str(tracks_count())


@app.route("/get_customers")
@use_kwargs({
        "city": fields.Str(
        required = False,
        missing = "%"
        ),
        "country": fields.Str(
        required = False,
        missing = "%"
        )}, 
        location = "query"
)
def get_customers(city, country):
    return customers(city,country)

@app.route("/get_sales")
def get_sales():
    return str(sales())


@app.route("/get_genre_durations")
def get_genre_durations():
    return genre_durations()


@app.route("/get_greatest_hits")
@use_kwargs({
        "count": fields.Int(
        required = False,
        missing = 0
        )}, 
        location = "query"
)
def get_greatest_hits(count):
    hits = greatest_hits()
    if count > 0:
        length = count
    else:
        length = len(hits.items())
    list = [v for i, v in enumerate(hits.items()) if i < length]
    songs = ""
    for i in list:
        songs += "".join(str(i))
        songs += "</p>" 
    return songs
    

app.run(debug=True,port=5004)
