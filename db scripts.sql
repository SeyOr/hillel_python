create table currency_statistics.bank
(
 id serial not null constraint bank_key primary key,
 full_name varchar(100) not null constraint bank_full_name_key unique,
 short_name varchar(100),
 discription varchar(1024),
 url varchar(1024),
 is_enabled boolean default true,
 created timestamp default current_timestamp
);

create table currency_statistics.currency
(
 id serial not null constraint currency_pkey primary key,
 international_name varchar(100) not null,
 current_name varchar(100) not null constraint currency_current_name_key unique,
 description varchar(1024),
 is_enabled boolean default false,
 created timestamp default CURRENT_TIMESTAMP
);

create table currency_statistics.parsers
(
 id serial not null constraint parsers_pkey primary key,
 bank_id bigint not null constraint parsers_bank_id_fkey references currency_statistics.bank,
 module_name varchar(256) not null,
 class_name varchar(256) not null,
 created timestamp default CURRENT_TIMESTAMP
);

create table currency_statistics.rate
(
 id serial not null constraint rate_pkey primary key,
 bank_id bigint not null constraint rate_bank_id_fkey references currency_statistics.bank,
 currency_id bigint not null constraint rate_currency_id_fkey references currency_statistics.currency,
 sale numeric(12,2) not null,
 purchase numeric(12,2) not null,
 created timestamp default CURRENT_TIMESTAMP
);


insert into currency_statistics.bank (id, full_name, short_name, discription, url)
values
    (1, 'PrivatBank', NULL, NULL, 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5'),
    (2, 'Raiffeisen Bank Aval', NULL, NULL, 'https://www.aval.ua/documents/currencies/kursy-valiut-v-kasakh-viddilen-banku'),
    (3, 'UkrSibbank', NULL, NULL, 'https://my.ukrsibbank.com/ua/personal/'),
    (4, 'Kit Group', NULL, NULL, 'https://obmenka.od.ua/');
   

insert into currency_statistics.currency (international_name, current_name, description, is_enabled)
values ('USD', 'usd', null, true), ('EUR', 'eur', null, true), ('RUB', 'rub', null, true), ('RUB', 'rur', null, true);

insert into currency_statistics.parsers (bank_id, module_name, class_name)
values (1, 'private_bank_parser', 'PrivateBankParser'), (2, 'aval_bank_parser', 'RaiffeisenBankAvalParser'),
       (3, 'ukrsib_bank_parser', 'UkrSibBankParser'), (4, 'kit_group_parser', 'KiGroupParser');
       
  
insert into currency_statistics.bank (id, full_name, short_name, discription, url)
values(5, 'MonoBank', null, null, 'https://api.monobank.ua/bank/currency');

insert into currency_statistics.parsers (bank_id, module_name, class_name)
values  (5,'mono_bank_parser', 'MonoBankParser');

insert into currency_statistics.bank (id, full_name, short_name, discription, url)
values(6, 'Pivdenniy', null, null, 'https://https://bank.com.ua/en');

insert into currency_statistics.parsers (bank_id, module_name, class_name)
values  (6,'pivdenniy_bank_parser', 'PivdenniyParser');
      